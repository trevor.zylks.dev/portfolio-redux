import React from 'react';
import logo from './logo.svg';
import { LandingPage } from './Components'
function App() {
  return (
    <div className="App">
      <LandingPage />
    </div>
  );
}

export default App;
