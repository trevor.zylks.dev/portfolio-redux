import ThemeProvider from '@mui/material/styles/ThemeProvider';
import { Container, Typography, Button } from '@mui/material';
import { theme } from '../Theme';

export const LandingPage = () => {
    return (
      <ThemeProvider theme={theme}>
        <Container sx={{
      backgroundColor: "#f7f7f7",
      minHeight: "100vh",
    }}>
          <Container sx={{padding: "2rem"}}>
            <Typography variant="h1" sx={{ color: "#ff5e57",
      marginBottom: "1rem"}}>
              Welcome to the 90s!
            </Typography>
            <Typography variant="h2" sx={{ color: "#9b9b9b",
      marginBottom: "2rem"}}>
              Come and enjoy the nostalgia of the 90s with us
            </Typography>
            <Button sx={{backgroundColor: "#ff5e57",
      color: "#fff",
      "&:hover": {
        backgroundColor: "#ff5e57",
      },}}>Learn More</Button>
          </Container>
        </Container>
      </ThemeProvider>
    );
}

