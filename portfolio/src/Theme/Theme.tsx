import {
    Container,
    Typography,
    Grid,
    makeStyles,
    createMuiTheme,
    ThemeProvider,
  } from "@mui/material";
  import { useEffect, useState } from "react";

  export const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#ff5e57"
      },
      secondary: {
        main: "#9b9b9b"
      },
    },
    typography: {
      fontFamily: "Arial, sans-serif",
      h1: {
        fontSize: "3rem",
        fontWeight: 800
      },
      h2: {
        fontSize: "2rem",
        fontWeight: 600
      },
      h3: {
        fontSize: "1.5rem",
        fontWeight: 400
      },
      h4: {
        fontSize: "1.25rem",
        fontWeight: 300
      },
      h5: {
        fontSize: "1rem",
        fontWeight: 200
      },
      h6: {
        fontSize: "0.875rem",
        fontWeight: 100
      },
      body1: {
        fontSize: "1rem",
        fontWeight: 400
      },
      body2: {
        fontSize: "0.875rem",
        fontWeight: 300
      },
      caption: {
        fontSize: "0.75rem",
        fontWeight: 300
      },
      button: {
        fontSize: "1rem",
        fontWeight: 10,
    },
  },
});

    